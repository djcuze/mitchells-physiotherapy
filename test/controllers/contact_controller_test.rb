require 'test_helper'

class ContactControllerTest < ActionDispatch::IntegrationTest
  test "should get feedback" do
    get contact_feedback_url
    assert_response :success
  end

end
