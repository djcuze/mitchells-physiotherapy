document.addEventListener("turbolinks:load", function () {

    var menu = $('header');

    $('.mobile-menu').click(function () {

        if ($(menu).css('display') == 'none') {
            menu.toggle().addClass('visible mobile');
        } else {
            menu.removeClass('visible mobile');
        }
        $(this).hide()
    });
    $(document).bind("mouseup touchend", function (e) {

        if (!menu.is(e.target) && menu.hasClass('mobile')
            && menu.has(e.target).length === 0) {
            menu.hide();
            $('.mobile-menu').show()
        }
    });
});
