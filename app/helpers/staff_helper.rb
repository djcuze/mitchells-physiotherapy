module StaffHelper

  def gravatar_for(staff)
    gravatar_id = Staff.name
    gravatar_url = "app/assets/images/avatars"
    image_tag(gravatar_url, alt: staff.name, class: "gravatar")
  end

end
