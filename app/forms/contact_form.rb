require_relative 'modeless_form'

class ContactForm < ModelessForm
  property :name
  property :contact_email
  property :message
end