require_relative 'modeless_form'

class FeedbackForm < ModelessForm
  property :treatment
  property :physiotherapist
  property :treatment_satisfaction
  property :recommendation
  property :receptionist
  property :receptionist_satisfaction
  property :comments
end