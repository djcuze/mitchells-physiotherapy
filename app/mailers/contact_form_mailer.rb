class ContactFormMailer < ApplicationMailer

  def contact_form_email(form)
    @form = form
    mail(
        to: Physio::Application.secrets.clinic_contact_email,
        subject: 'Generic Online Contact Message' + ' - ' + Time.now.strftime("%D:%M:%S")
    )
  end

end