class ModelessForm < Reform::Form

  def initialize
    super(OpenStruct.new)
  end

  def self.property(name, options={})
    super(name, options.merge(virtual: true)) # every property needs to be virtual.
  end

end