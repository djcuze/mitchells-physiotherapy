class StaffMembersController < ApplicationController
  def index
    @staff_member = StaffMember.all
  end

  def show
    @staff_member = StaffMember.find(params[:id])
  end

  def new
    @staff_member = StaffMember.new
  end

  def edit
    @staff_member = StaffMember.find(params[:id])
  end

  def create
    ## Controller Parameters have to be whitelisted to prevent "WRONGFUL MASS ASSIGNMENT"
    @staff_member = StaffMember.new(staff_member_params)

    if @staff_member.save
      redirect_to @staff_member
    else
      render 'new'
    end
  end

  def update
    @staff_member = StaffMember.find(params[:id])

    if @staff_member.update(staff_member_params)
      redirect_to @staff_member
    else
      render 'edit'
    end
  end

  def destroy
    @staff_member = StaffMember.find(params[:id])
    @staff_member.destroy

    redirect_to staff_members_path
  end

  private
  def staff_member_params
    params.require(:staff_member).permit(:name, :qualifications, :biography)
  end
end
