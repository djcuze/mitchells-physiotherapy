class ApplicationMailer < ActionMailer::Base
  default from: Physio::Application.secrets.contact_email
  layout 'mailer'
end