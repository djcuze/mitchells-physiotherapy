class FeedbackFormMailer < ApplicationMailer

  def feedback_form_email(form)
    @form = form
    mail(
        to: Physio::Application.secrets.contact_email,
        subject: 'Customer Feedback' + ' - ' + Time.now.strftime("%D:%M:%S")
    )
  end

end