require 'test_helper'

class StaffTest < ActiveSupport::TestCase
  def setup
    @user = Staff.new(name: 'Example User', :qualifications => "Bachelor of Frikkin' Awesome!", bio: "Something cool here but probably with an apostrophe'")
  end

  test 'should be valid' do
    assert @user.valid?
  end

  test 'name should be present' do
    @user.name = '     '
    assert_not @user.valid?
  end

  test 'qualifications should be present' do
    @user.qualifications = ' '
    assert_not @user.valid?
  end

  test 'A Biography should be present' do
    @user.bio = " "
    assert_not @user.valid?
  end
end
