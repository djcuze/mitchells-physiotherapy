module NavigationHelper
  def home_link(html_options = {})
    get_link_for('Home', root_path)
  end

  def treatment_link(html_options = {})
    get_link_for('Treatment', treatment_path)
  end

  def staff_link(html_options = {})
    get_link_for 'Staff', staff_path
  end

  def consulting_link(html_options = {})
    get_link_for 'Consulting', consulting_path
  end

  def about_link(html_options = {})
    get_link_for 'About', about_path
  end

  def contact_link(html_options = {})
    options = is_contact_path? ? { class: 'active' } : {}
    link_to 'Contact', contact_path, options
  end

  def contact_feedback_link
    link_to 'Feedback', contact_feedback_path
  end

  def contact_quote_link
    link_to 'Online Quote', contact_quote_path
  end

  def get_link_for(text, path)
    options = current_page?(path) ? { class: 'active' } : {}
    link_to text, path, options
  end

  def is_contact_path?
    current_page?(contact_path) ||
        current_page?(contact_feedback_path) ||
        current_page?(contact_quote_path)
  end
end
