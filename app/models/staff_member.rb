class StaffMember < ApplicationRecord
  validates(:name, presence: true)
  validates(:qualifications, presence: true)
  validates(:biography, presence: true)
end
