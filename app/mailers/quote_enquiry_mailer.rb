class QuoteEnquiryMailer < ApplicationMailer

  def quote_enquiry_email(form)
    @form = form
    mail(
        to: Physio::Application.secrets.contact_email,
        subject: 'Quote Requested'
    )
  end

end