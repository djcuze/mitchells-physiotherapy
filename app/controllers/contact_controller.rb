class ContactController < ApplicationController

  def index
    @contact_form = ContactForm.new
  end

  def feedback

  end

  def feedback_form
    @feedback_form = FeedbackForm.new
  end

  def complete_contact_form
    @contact_form = ContactForm.new

    if @contact_form.validate(params[:contact_form])
      ContactFormMailer.contact_form_email(@contact_form).deliver_now
      redirect_to :contact_form, notice: 'Your message has been submitted successfully'
    else
      render :contact_form;
    end
  end

  def complete_feedback_form
    @feedback_form = FeedbackForm.new

    if @feedback_form.validate(params[:feedback_form])
      FeedbackFormMailer.feedback_form_email(@feedback_form).deliver_now
      redirect_to :feedback_form, notice: 'Your function enquiry has been submitted successfully'
    else
      render :feedback_form;
    end
  end

  def quote

  end

  def quote_enquiry
    @quote_enquiry = QuoteEnquiry.new
  end

  def complete_enquiry_form
    @quote_enquiry = QuoteEnquiry.new

    if @quote_enquiry.validate(params[:quote_enquiry])
      QuoteEnquiryMailer.quote_enquiry_email(@quote_enquiry).deliver_now
      redirect_to :quote_enquiry, notice: 'Your Quote Enquiry has been submitted successfully'
    else
      render :quote_enquiry;
    end
  end

  private

  def test

  end

end
