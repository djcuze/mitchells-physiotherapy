require_relative 'modeless_form'

class QuoteEnquiry < ModelessForm
  property :size
  property :services
  property :month
  property :contact
  property :name
  property :email
  property :phone

  def services
    super.reject { |attr| attr == '0' }
  end

  validates(:name, presence: true)
  validates(:email, presence: true)
  validates(:phone, presence: true, length: {minimum: 8, maximum: 10})
  validates(:contact, presence: true)
  validates(:size, presence: true)
  validates(:services, presence: true)
  validates(:month, presence: true)
end
