document.addEventListener("turbolinks:load", function() {

    var fieldsets = $('#feedbackForm, #quote').find('fieldset');
    var progressIndicators = $('.progress > li');
    var currentIndex = fieldsets.index('.show');

    $('.progress-button').each(function () {
        $(this).on('click', function () { // On Click of the 'Next' button

            if (window.e === undefined) {
                $(this).attr('disabled', false)
            }
            else {
                hideFieldset();
                currentIndex += 1;
                showFieldset();
                $('.output').append('<li>' + window.e + '</li>');
                $('.form-progression').html(currentIndex + 1);
                window.e = undefined;
            }
        });
    });

    function hideFieldset() {
        $(fieldsets[currentIndex])
            .removeClass('show');
        $(progressIndicators[currentIndex])
            .removeClass('active')
            .addClass('completed');
    }

    function showFieldset() {
        $(fieldsets[currentIndex])
            .addClass('show');
        $(progressIndicators[currentIndex])
            .addClass('active');
    }

    // Correctly returns which radio button has been clicked
    $('input:radio').each(function () {
        $(this).change(function () {
            window.e = $(this).val();
        });
    });

    // Handles 'Other' Text Field
    $('input.other').focus(function() {
       $('input:radio#feedback_form_treatment_other').prop('checked', true);
    });

    $('input.other').change(function() {
        window.e = $(this).val();
    });

    // Handles Checkboxes
    $('input:checkbox').each(function () {
        $(this).change(function() {
            window.e = $(this).val();
            console.log(window.e)
        })
    });

    // Handles Dropdown Selection
    $('select').each(function() {
        $(this).change(function() {
            window.e = ($('option:selected').text())
        });
    });


});
