module ApplicationHelper

  def get_banner_content
    if current_page?('/staff')
      @header = 'Our Team'
      @subtext = 'Of Highly Trained Professionals'
    elsif current_page?(controller: 'treatment', action: 'index')
      @header = 'In-House Treatment'
      @subtext = 'Professional Service for all Individual Needs'
    elsif current_page?(controller: 'index', action: 'about')
      @header = 'About Us'
      @subtext = 'Operating in Greenslopes for over 25 years'
    elsif current_page?(controller: 'contact', action: 'index')
      @header = 'Contact Us'
      @subtext ='How can we help you?'
    elsif current_page?(controller: 'contact', action: 'quote')
      @header = 'Online Quote'
      @subtext = 'Testing'
    end
  end

end