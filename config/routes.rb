Rails.application.routes.draw do
  root 'index#home'

  get '/treatment' => 'treatment#index'
  get '/consulting' => 'consulting#index'
  get '/about' => 'index#about'
  get '/contact' => 'contact#index'
  get '/feedback' => 'contact#feedback'
  get '/quote' => 'contact#quote'


  # Staff
  resources :staff_members

  get '/staff' => 'staff_members#index'


  # Contact
  get 'contact/feedback'
  get 'contact/quote'
  post '/contact' => 'contact#complete_contact_form', as: :contact_form

  resources :feedbackforms

  get 'feedback_forms/index'
  get 'feedback_forms/new'
  get 'feedback_forms/create'
  post '/feedback' => 'contact#complete_feedback_form', as: :feedback_form

  resources :quoteenquiries

  get 'quote_enquiry/index'
  get 'quote_enquiry/new'
  get 'quote_enquiry/create'
  post '/quote' => 'contact#complete_enquiry_form', as: :quote_enquiry

  get '/test' => 'contact#test'
end
